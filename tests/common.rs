pub use whyml::ast::*;
pub use whyml::*;

pub fn print_to_string<P: whyml::PrettyPrint>(val: &P) -> String {
    let mut output = vec![];

    whyml::print(&mut output, val).unwrap();

    String::from_utf8(output).unwrap()
}
