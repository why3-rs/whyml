mod common;
use common::*;

#[test]
fn empty() {
    let name = UIdent::new("Test");
    let module = Module {
        name,
        items: vec![],
    };

    let res = print_to_string(&module);

    assert_eq!(
        format!("\n{}", res),
        r#"
module Test
end
"#
    );
}

#[test]
fn with_use() {
    let name = UIdent::new("Test");
    let module = Module {
        name,
        items: vec![Item::Use(Use {
            path: TQualId::new_with_prefix(
                vec![Ident::new("ref")],
                UIdent::new("Ref"),
            ),
            rename: None,
        })],
    };

    let res = print_to_string(&module);

    assert_eq!(
        format!("\n{}", res),
        r#"
module Test
    use ref.Ref
end
"#
    );
}

#[test]
fn with_goal_infix() {
    let name = UIdent::new("Test");
    let module = Module {
        name,
        items: vec![Item::Goal(Goal {
            name: UIdent::new("G0"),
            expr: Expr::Infix {
                lhs: Box::new(Expr::Integer(5)),
                operator: InfixOp::new("="),
                rhs: Box::new(Expr::Integer(5)),
            },
        })],
    };

    let res = print_to_string(&module);

    assert_eq!(
        format!("\n{}", res),
        r#"
module Test
    goal G0:
        (5 = 5)
end
"#
    );
}

#[test]
fn with_goal_sequence() {
    let name = UIdent::new("Test");
    let module = Module {
        name,
        items: vec![Item::Goal(Goal {
            name: UIdent::new("G0"),
            expr: Expr::Sequence {
                lhs: Box::new(Expr::Integer(5)),
                rhs: Box::new(Expr::Integer(6)),
            },
        })],
    };

    let res = print_to_string(&module);

    assert_eq!(
        format!("\n{}", res),
        r#"
module Test
    goal G0:
        5;
        6
end
"#
    );
}
