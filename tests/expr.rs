mod common;
use common::*;

#[test]
fn integer() {
    let num = Expr::Integer(42);

    let res = print_to_string(&num);

    assert_eq!(res, "42");
}

#[test]
fn real() {
    let num = Expr::Real(13.37);

    let res = print_to_string(&num);

    assert_eq!(res, "13.37");
}

#[test]
fn variable() {
    let num = Expr::Symbol(LQualId::new(Ident::new("test")));

    let res = print_to_string(&num);

    assert_eq!(res, "test");
}

#[test]
fn prefix_simple_neg() {
    let num = Expr::Prefix {
        operator: PrefixOp::new("-"),
        expr: Box::new(Expr::Integer(2)),
    };

    let res = print_to_string(&num);

    assert_eq!(res, "(- 2)");
}

#[test]
fn infix_simple_eq() {
    let num = Expr::Infix {
        lhs: Box::new(Expr::Integer(1)),
        operator: InfixOp::new("<>"),
        rhs: Box::new(Expr::Integer(2)),
    };

    let res = print_to_string(&num);

    assert_eq!(res, "(1 <> 2)");
}

#[test]
fn bracket() {
    let num = Expr::Bracket {
        base: Box::new(Expr::Symbol(LQualId::new(Ident::new("test")))),
        index: Box::new(Expr::Integer(1)),
    };

    let res = print_to_string(&num);

    assert_eq!(res, "(test[1])");
}

#[test]
fn bracket_assignment() {
    let num = Expr::BracketAssignment {
        base: Box::new(Expr::Symbol(LQualId::new(Ident::new("test")))),
        index: Box::new(Expr::Integer(1)),
        rhs: Box::new(Expr::Integer(42)),
    };

    let res = print_to_string(&num);

    assert_eq!(res, "(test[1] <- 42)");
}

#[test]
fn bracket_ternary() {
    let num = Expr::BracketTernary {
        base: Box::new(Expr::Symbol(LQualId::new(Ident::new("test")))),
        index_lhs: Box::new(Expr::Integer(1)),
        index_op: InfixOp::new("<-"),
        index_rhs: Box::new(Expr::Integer(42)),
    };

    let res = print_to_string(&num);

    assert_eq!(res, "(test[1 <- 42])");
}

#[test]
fn call_simple() {
    let num = Expr::Call {
        base: Box::new(Expr::Symbol(LQualId::new(Ident::new("foo")))),
        arguments: vec![Expr::Integer(1), Expr::Integer(2)],
    };

    let res = print_to_string(&num);

    assert_eq!(res, "(foo 1 2)");
}

#[test]
fn sequence_simple_numbers() {
    let num = Expr::Sequence {
        lhs: Box::new(Expr::Integer(1)),
        rhs: Box::new(Expr::Integer(2)),
    };

    let res = print_to_string(&num);

    assert_eq!(res, "1;\n2");
}
