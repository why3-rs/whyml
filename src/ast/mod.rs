use crate::PrettyPrint;

pub mod expr;
pub mod item;

pub use self::expr::*;
pub use self::item::*;

/// A lowercase identifier.
#[derive(Debug, Clone)]
pub struct Ident(pub String);

impl Ident {
    pub fn new<S: Into<String>>(name: S) -> Self {
        Ident(name.into())
    }
}

/// An uppercase identifier.
#[derive(Debug, Clone)]
pub struct UIdent(pub String);

impl UIdent {
    pub fn new<S: Into<String>>(name: S) -> Self {
        UIdent(name.into())
    }
}

/// A path with a lowercase identifier, possibly prefixed by a single uppercase
/// identifier.
#[derive(Debug, Clone)]
pub struct LQualId {
    prefix: Option<UIdent>,
    name: Ident,
}

impl LQualId {
    pub fn new(name: Ident) -> Self {
        LQualId { prefix: None, name }
    }

    pub fn new_with_prefix(prefix: UIdent, name: Ident) -> Self {
        LQualId {
            prefix: Some(prefix),
            name,
        }
    }
}

/// A path with an uppercase identifier, possibly prefixed by a number of
/// lowercase identifiers.
#[derive(Debug, Clone)]
pub struct TQualId {
    prefix: Vec<Ident>,
    name: UIdent,
}

impl TQualId {
    pub fn new(name: UIdent) -> Self {
        TQualId {
            prefix: vec![],
            name,
        }
    }
    pub fn new_with_prefix<P>(prefix: P, name: UIdent) -> Self
    where
        P: IntoIterator<Item = Ident>,
    {
        TQualId {
            prefix: prefix.into_iter().collect(),
            name,
        }
    }
}

impl PrettyPrint for Ident {
    fn print_level<W: std::io::Write>(
        &self,
        _level: usize,
        output: &mut W,
    ) -> Result<(), std::io::Error> {
        write!(output, "{}", self.0)
    }
}

impl PrettyPrint for UIdent {
    fn print_level<W: std::io::Write>(
        &self,
        _level: usize,
        output: &mut W,
    ) -> Result<(), std::io::Error> {
        write!(output, "{}", self.0)
    }
}

impl PrettyPrint for LQualId {
    fn print_level<W: std::io::Write>(
        &self,
        _level: usize,
        output: &mut W,
    ) -> Result<(), std::io::Error> {
        if let Some(prefix) = &self.prefix {
            write!(output, "{}.", prefix.0)?;
        }
        write!(output, "{}", self.name.0)
    }
}

impl PrettyPrint for TQualId {
    fn print_level<W: std::io::Write>(
        &self,
        _level: usize,
        output: &mut W,
    ) -> Result<(), std::io::Error> {
        for ident in &self.prefix {
            write!(output, "{}.", ident.0)?;
        }

        write!(output, "{}", self.name.0)
    }
}
