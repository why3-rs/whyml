use crate::ast::LQualId;
use crate::{indent, PrettyPrint};

#[derive(Debug, Clone)]
pub enum Expr {
    Integer(u128),
    Real(f64),

    Symbol(LQualId),

    Prefix {
        operator: PrefixOp,
        expr: Box<Expr>,
    },
    Infix {
        lhs: Box<Expr>,
        operator: InfixOp,
        rhs: Box<Expr>,
    },

    Bracket {
        base: Box<Expr>,
        index: Box<Expr>,
    },

    BracketAssignment {
        base: Box<Expr>,
        index: Box<Expr>,
        rhs: Box<Expr>,
    },
    BracketTernary {
        base: Box<Expr>,
        index_lhs: Box<Expr>,
        index_op: InfixOp,
        index_rhs: Box<Expr>,
    },

    Call {
        base: Box<Expr>,
        arguments: Vec<Expr>,
    },

    Sequence {
        lhs: Box<Expr>,
        rhs: Box<Expr>,
    },
}

#[derive(Debug, Clone)]
pub struct InfixOp(pub String);

impl InfixOp {
    pub fn new<S>(operator: S) -> Self
    where
        S: AsRef<str>,
    {
        InfixOp(operator.as_ref().to_string())
    }
}

#[derive(Debug, Clone)]
pub struct PrefixOp(pub String);

impl PrefixOp {
    pub fn new<S>(operator: S) -> Self
    where
        S: AsRef<str>,
    {
        PrefixOp(operator.as_ref().to_string())
    }
}

impl PrettyPrint for Expr {
    fn print_level<W: std::io::Write>(
        &self,
        cur_level: usize,
        output: &mut W,
    ) -> Result<(), std::io::Error> {
        match self {
            Expr::Integer(n) => write!(output, "{}", n),
            Expr::Real(n) => write!(output, "{}", n),
            Expr::Symbol(ident) => ident.print_level(cur_level, output),
            Expr::Prefix { operator, expr } => {
                write!(output, "(")?;

                operator.print_level(cur_level + 1, output)?;
                write!(output, " ")?;
                expr.print_level(cur_level + 1, output)?;

                write!(output, ")")
            }
            Expr::Infix { lhs, operator, rhs } => {
                write!(output, "(")?;

                lhs.print_level(cur_level + 1, output)?;
                write!(output, " ")?;
                operator.print_level(cur_level + 1, output)?;
                write!(output, " ")?;
                rhs.print_level(cur_level + 1, output)?;

                write!(output, ")")
            }

            Expr::Bracket { base, index } => {
                write!(output, "(")?;
                base.print_level(cur_level + 1, output)?;
                write!(output, "[")?;

                index.print_level(cur_level + 1, output)?;

                write!(output, "]")?;
                write!(output, ")")
            }

            Expr::BracketAssignment { base, index, rhs } => {
                write!(output, "(")?;
                base.print_level(cur_level + 1, output)?;
                write!(output, "[")?;

                index.print_level(cur_level + 1, output)?;

                write!(output, "] <- ")?;

                rhs.print_level(cur_level + 1, output)?;

                write!(output, ")")
            }
            Expr::BracketTernary {
                base,
                index_lhs,
                index_op,
                index_rhs,
            } => {
                write!(output, "(")?;
                base.print_level(cur_level + 1, output)?;
                write!(output, "[")?;

                index_lhs.print_level(cur_level + 1, output)?;
                write!(output, " ")?;
                index_op.print_level(cur_level + 1, output)?;
                write!(output, " ")?;
                index_rhs.print_level(cur_level + 1, output)?;

                write!(output, "]")?;
                write!(output, ")")
            }

            Expr::Call { base, arguments } => {
                write!(output, "(")?;

                base.print_level(cur_level, output)?;

                for arg in arguments {
                    write!(output, " ")?;
                    arg.print_level(cur_level, output)?;
                }

                write!(output, ")")
            }
            Expr::Sequence { lhs, rhs } => {
                lhs.print_level(cur_level, output)?;
                writeln!(output, ";")?;
                indent(cur_level, output)?;
                rhs.print_level(cur_level, output)
            }
        }
    }
}

impl PrettyPrint for InfixOp {
    fn print_level<W: std::io::Write>(
        &self,
        _cur_level: usize,
        output: &mut W,
    ) -> Result<(), std::io::Error> {
        write!(output, "{}", self.0)
    }
}

impl PrettyPrint for PrefixOp {
    fn print_level<W: std::io::Write>(
        &self,
        _cur_level: usize,
        output: &mut W,
    ) -> Result<(), std::io::Error> {
        write!(output, "{}", self.0)
    }
}
