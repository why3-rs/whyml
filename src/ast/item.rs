use crate::{indent, PrettyPrint};

use crate::ast::{Expr, TQualId, UIdent};

#[derive(Debug, Clone)]
pub struct Module {
    pub name: UIdent,
    pub items: Vec<Item>,
}

#[derive(Debug, Clone)]
pub enum Item {
    Use(Use),
    Goal(Goal),
    Lemma(Lemma),
}

#[derive(Debug, Clone)]
pub struct Use {
    pub path: TQualId,
    pub rename: Option<UIdent>,
}

#[derive(Debug, Clone)]
pub struct Goal {
    pub name: UIdent,
    pub expr: Expr,
}

#[derive(Debug, Clone)]
pub struct Lemma {
    pub name: UIdent,
    pub expr: Expr,
}

impl PrettyPrint for Item {
    fn print_level<W: std::io::Write>(
        &self,
        cur_level: usize,
        output: &mut W,
    ) -> Result<(), std::io::Error> {
        match self {
            Item::Use(use_) => use_.print_level(cur_level, output),
            Item::Goal(goal) => goal.print_level(cur_level, output),
            Item::Lemma(lemma) => lemma.print_level(cur_level, output),
        }
    }
}

impl PrettyPrint for Use {
    fn print_level<W: std::io::Write>(
        &self,
        cur_level: usize,
        output: &mut W,
    ) -> Result<(), std::io::Error> {
        write!(output, "use ")?;
        self.path.print_level(cur_level + 1, output)?;

        if let Some(rename) = &self.rename {
            write!(output, " as ")?;
            rename.print_level(cur_level + 1, output)?;
        }
        writeln!(output)
    }
}

impl PrettyPrint for Goal {
    fn print_level<W: std::io::Write>(
        &self,
        cur_level: usize,
        output: &mut W,
    ) -> Result<(), std::io::Error> {
        write!(output, "goal ")?;
        self.name.print_level(cur_level + 1, output)?;
        writeln!(output, ":")?;
        indent(cur_level + 1, output)?;
        self.expr.print_level(cur_level + 1, output)?;
        writeln!(output)
    }
}

impl PrettyPrint for Lemma {
    fn print_level<W: std::io::Write>(
        &self,
        cur_level: usize,
        output: &mut W,
    ) -> Result<(), std::io::Error> {
        write!(output, "lemma ")?;
        self.name.print_level(cur_level + 1, output)?;
        writeln!(output, ":")?;
        indent(cur_level + 1, output)?;
        self.expr.print_level(cur_level + 1, output)?;
        writeln!(output)
    }
}

impl PrettyPrint for Module {
    fn print_level<W: std::io::Write>(
        &self,
        cur_level: usize,
        output: &mut W,
    ) -> Result<(), std::io::Error> {
        write!(output, "module ")?;
        self.name.print_level(cur_level + 1, output)?;
        write!(output, "\n")?;

        for item in &self.items {
            indent(cur_level + 1, output)?;
            item.print_level(cur_level + 1, output)?;
        }

        indent(cur_level, output)?;
        writeln!(output, "end")
    }
}
