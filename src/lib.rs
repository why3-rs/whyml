#![forbid(unsafe_code)]
#![warn(unused_results)]
#![warn(missing_debug_implementations)]

pub mod ast;

pub fn print<T, W>(output: &mut W, value: &T) -> std::io::Result<()>
where
    T: PrettyPrint,
    W: std::io::Write,
{
    value.print_level(0, output)
}

pub trait PrettyPrint {
    fn print_level<W: std::io::Write>(
        &self,
        cur_level: usize,
        output: &mut W,
    ) -> Result<(), std::io::Error>;
}

pub(crate) fn indent<W>(level: usize, output: &mut W) -> std::io::Result<()>
where
    W: std::io::Write,
{
    for _ in 0..level {
        write!(output, "    ")?;
    }
    Ok(())
}
